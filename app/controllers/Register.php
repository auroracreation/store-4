<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\User;

class Register extends Controller
{

    public function registerForm()
    {
        $this->view('home/index');
    }

    public $user;

    public function __construct()
    {
        $this->user = new User();
    }


    /**
     * add new user
     */

    public function add()
    {
        $this->user->setName($_POST['name']);
        $this->user->setSurname($_POST['surname']);
        $this->user->setEmail($_POST['email']);
        $this->user->setPassword($_POST['password']);
        $this->user->add();
        $this->redirect("Location: http://localhost/mvc/public/register/registerform");


    }

    /**
     * delete user
     */
    public function delete()
    {
        $this->user->setId($_POST['id']);
        $this->user->delete();

        $this->redirect("Location: http://localhost/mvc/public/table/viewtable");
    }


}
